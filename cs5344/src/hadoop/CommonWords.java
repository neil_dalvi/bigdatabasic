package hadoop;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.collections.SetUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class CommonWords {
	
	@SuppressWarnings("unchecked")
	private static Set<String> setupStopWords() {
		Set<String> stopwords = new HashSet<String>();
		try {
			FileSystem fileSystem = FileSystem.get(new Configuration());
			BufferedReader br = new BufferedReader(new InputStreamReader(fileSystem.open(new Path("lab1/data/sw4.txt"))));
			String word;
			while((word = br.readLine()) != null) {
				stopwords.add(word.trim());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SetUtils.EMPTY_SET;
	}

	// tokenize file 1
	public static class TokenizerWCMapper1 extends
			Mapper<Object, Text, Text, Text> {

		Set<String> stopwords = new HashSet<String>();		

		@Override
		protected void setup(Context context) {
			stopwords = setupStopWords();
		}

		private Text word = new Text();
		private final static Text identifier = new Text("f1");

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			StringTokenizer itr = new StringTokenizer(value.toString(), "!:*.,#\"'$()0123456789 \t\n\r\f");
			while (itr.hasMoreTokens()) {
				word.set(itr.nextToken());
				//check whether the word is in stopword list
				if(stopwords.contains(word)) continue;

				//write out <word, f1>
				context.write(word, identifier);

			}
		}
	}

	// tokenize file 2
	public static class TokenizerWCMapper2 extends
			Mapper<Object, Text, Text, Text> {

		Set<String> stopwords = new HashSet<String>();

		@Override
		protected void setup(Context context) {
			//Read stopwords from HDFS and put it into the Set<String> stopwords
			stopwords = setupStopWords();
		}

		private Text word = new Text();
		private final static Text identifier = new Text("f2");

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			StringTokenizer itr = new StringTokenizer(value.toString(), "!:*.,#\"$()0123456789 \t\n\r\f");
			while (itr.hasMoreTokens()) {
				word.set(itr.nextToken());
				//check whether the word is in stopword list
				if(stopwords.contains(word.toString())) continue;
				//write out <word, f2>
				context.write(word, identifier);
			}
		}
	}
	
	// get the number of common words
	public static class CommonWordsReducer extends
			Reducer<Text, Text, Text, IntWritable> {

		private IntWritable commoncount = new IntWritable();
		private final static Text identifier1 = new Text("f1");
		private final static Text identifier2 = new Text("f2");

		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			//maintain two counts for file 1 and file 2
			int count1 = 0;
			int count2 = 0;
			for (Text val : values) {
				// increase count1 or count2
				if(val.equals(identifier1)) {
					count1++;
				}
				if(val.equals(identifier2)){
					count2++;
				}
			}
			if (count1 != 0 && count2 != 0) {
				//It is a common word here. Output its name and count
				commoncount.set(Math.min(count1, count2));
				context.write(key, commoncount);
			}
		}
	}

	public static class SortMapper extends
			Mapper<Object, Text, IntWritable, Text> {

		private IntWritable count = new IntWritable();
		private Text word = new Text();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			//output the <word, count> as <count, word>
			//int countIntValue = Integer.parseInt(value.toString());
			//count.set(countIntValue);
			StringTokenizer tokenizer = new StringTokenizer(value.toString());
			word.set(tokenizer.nextToken());
			count.set(Integer.MAX_VALUE-Integer.parseInt(tokenizer.nextToken()));
			context.write(count, word);
		}
	}

	public static class SortReducer extends
			Reducer<IntWritable, Text, IntWritable, Text> {

		public void reduce(IntWritable key, Iterable<Text> values,
				Context context) throws IOException, InterruptedException {
			//write out <key (word frequency) and value (word)>
			context.write(key, values.iterator().next());
		}
	}

	public static void main(String[] args) throws IOException,
			InterruptedException, ClassNotFoundException {
		// Provide filenames
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		if (otherArgs.length != 4) {
			System.err
					.println("Usage: TopKCommonWords <input1> <input2> <output1> "
							+ "<output2>");
			System.exit(2);
		}
		//conf.set("TopKCommonWords", otherArgs[0]);

		//Configure job 1 for counting common words
		Job job1 = new Job(conf, "Count Commond Words");
		job1.setJarByClass(CommonWords.class);
		
		//set input1 and input2 as input and <output1> as outputfor this MapReduce job.
		MultipleInputs.addInputPath(job1, new Path(otherArgs[0]), TextInputFormat.class, TokenizerWCMapper2.class);
		MultipleInputs.addInputPath(job1, new Path(otherArgs[1]), TextInputFormat.class, TokenizerWCMapper1.class);
		FileOutputFormat.setOutputPath(job1, new Path(otherArgs[2]));

		//set Mapper and Reduce class, output key, value class
		
		// Mappers were set during the declaration on inputs.
		//job1.setMapperClass(TokenizerWCMapper1.class);
		//job1.setMapperClass(TokenizerWCMapper2.class);
		
		job1.setReducerClass(CommonWordsReducer.class);
		job1.setMapOutputKeyClass(Text.class);
		job1.setMapOutputValueClass(Text.class);
		job1.setOutputKeyClass(Text.class);
		job1.setOutputValueClass(IntWritable.class);
		job1.setNumReduceTasks(1);
		

		job1.waitForCompletion(true);

		//Configure job 2 for sorting
		Job job2 = new Job(conf, "sort");
		job2.setJarByClass(CommonWords.class);
		
		//set input and output for MapReduce job 2 here
		FileInputFormat.setInputPaths(job2, new Path(otherArgs[2]));
		FileOutputFormat.setOutputPath(job2, new Path(otherArgs[3]));

		//set Mapper and Reduce class, output key, value class 
		job2.setMapperClass(SortMapper.class);
		job2.setReducerClass(SortReducer.class);
		job2.setMapOutputKeyClass(IntWritable.class);
		job2.setMapOutputValueClass(Text.class);
		job2.setOutputKeyClass(IntWritable.class);
		job2.setOutputValueClass(Text.class);

		System.exit(job2.waitForCompletion(true) ? 0 : 1);
	}
}