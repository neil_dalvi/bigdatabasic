package hadoop.verify;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class CommonWordsOutputCheck {

	public static void main(String[] args) throws IOException {
		
		/*if(args.length != 4) {
			System.out.println("CommonWordsOutputCheck usuage : pathToStopWords input1Path input2Path outputPath");
			System.exit(0);
		}*/
		
		String pathToStopWords = "/home/hadoop/workspace/cs5344/Lab1/data/sw4.txt";//args[0];
		String pathToInput1 = "/home/hadoop/workspace/cs5344/Lab1/data/input1.txt";//args[1];
		String pathToInput2 = "/home/hadoop/workspace/cs5344/Lab1/data/input2.txt";//args[2];
		//String outputPath = "home/hadoop/workspace/cs5344/Lab1/data/output.txt";
		
		HashSet<String> stopwords = fetchStopWords(pathToStopWords);
		
		Map<String, Integer> wordCountForFile1 = createCountMap(pathToInput1, stopwords);
		Map<String, Integer> wordCountForFile2 = createCountMap(pathToInput2, stopwords);
		
		final Map<String, Integer> commonWordCount = calcCommonWords(
				wordCountForFile1, wordCountForFile2);
		
		List<String> orderedWords = sortWordsDescByCount(commonWordCount);
		
		int count = 0;
		for(String word : orderedWords) {
			System.out.println(word + "\t" + commonWordCount.get(word));
			count++;
			if(count==20) break;
		}
		
	}

	private static List<String> sortWordsDescByCount(
			final Map<String, Integer> commonWordCount) {
		List<String> orderedWords = new ArrayList<String>(commonWordCount.keySet());
		Collections.sort(orderedWords, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				if(commonWordCount.get(o1) < commonWordCount.get(o2)) {
					return 1;
				}
				return 0;
			}
			
		});
		return orderedWords;
	}

	private static Map<String, Integer> calcCommonWords(
			Map<String, Integer> wordCountForFile1,
			Map<String, Integer> wordCountForFile2) {
		Map<String, Integer> commonWordCount = new HashMap<String, Integer>();
		
		for(String word : wordCountForFile1.keySet()) {
			if(wordCountForFile2.containsKey(word)) {
				commonWordCount.put(word, Math.min(wordCountForFile1.get(word), wordCountForFile2.get(word)));
			}
		}
		return commonWordCount;
	}

	private static Map<String, Integer> createCountMap( String path, HashSet<String> stopwords)
			throws FileNotFoundException, IOException {
		Map<String/*word*/, Integer/*count*/> wordCountForFile1 = new HashMap<String, Integer>();
		
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String line, word;
		while((line = br.readLine()) !=  null) {
			StringTokenizer tokenizer = new StringTokenizer(line, "!:*.,#\"'$()0123456789 \t\n\r\f");
			while(tokenizer.hasMoreTokens()){
				word = tokenizer.nextToken();
				if(stopwords.contains(word)) continue;
				if(wordCountForFile1.containsKey(word)) {
					wordCountForFile1.put(word, wordCountForFile1.get(word)+1);
				} else {
					wordCountForFile1.put(word, 1);
				}
			}
		}
		
		if(br != null) {
			br.close();
		}
		return wordCountForFile1;
	}

	private static HashSet<String> fetchStopWords(String path)
			throws FileNotFoundException, IOException {
		HashSet<String> stopwords = new HashSet<String>();
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String word;
		while((word = br.readLine()) !=  null) {
			stopwords.add(word);
		}
		
		if(br != null) {
			br.close();
		}
		return stopwords;
	}
}
